alberta (3.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream: Uni Stuttgart's GitLab
  * Update to debhelper-compat 12

  [ Tiago Stürmer Daitx ]
  * Fix FTBFS (Closes: #980015):
    - debian/control: add libtirpc-dev to bdeps.
    - debian/patches/001-autoconf-check-libtirpc.patch: check for
      libtirpc in configure.ac.
    - debian/rules: include tirpc in cflags.

 -- Bastian Germann <bage@debian.org>  Fri, 22 Oct 2021 13:49:17 +0200

alberta (3.0.1-2) unstable; urgency=medium

  * Drop transitional package libalberta2-dev. (Closes: #878199)
  * Switch to automatic -dbgsym packages and drop libalberta-dbg.
  * debian/control: Use `Rules-Requires-Root: no`.
  * debian/control: Update Vcs-* fields for move to salsa.d.o.
  * Bumped Standards-Version to 4.4.1.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 13 Oct 2019 11:58:26 +0200

alberta (3.0.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch fix-pkg-config.diff: applied upstream.
  * debian/copyright: Drop note about old license in demo/COPYING.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 01 Aug 2014 00:30:26 +0200

alberta (3.0.0-3) unstable; urgency=medium

  * Disable stack protector when building with clang. Otherwise the shared
    library end with an undefined reference to __stack_chk_guard.

 -- Ansgar Burchardt <ansgar@debian.org>  Fri, 13 Jun 2014 18:24:56 +0200

alberta (3.0.0-2) unstable; urgency=medium

  * Use clang on PowerPC. gcc cannot fold IBM's 128bit long doubles that
    are used on PowerPC and fails to compile statements like
        static const double a = 1.0L / 3.0L;
    Reference: <https://gcc.gnu.org/bugzilla/show_bug.cgi?id=26374>
  * debian/rules: Enable parallel build.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 13 May 2014 23:32:22 +0200

alberta (3.0.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #742013)
  * New maintainer. (Closes: #731839)
  * Source package renamed to alberta (from libalberta2).
  * Packaging redone from scratch.

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 23 Apr 2014 21:42:15 +0200
